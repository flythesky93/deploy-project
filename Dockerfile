FROM golang:alpine3.19
WORKDIR /app

# copy all code to container
COPY . .

#build project to run file
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o main

CMD ["/app/main"]