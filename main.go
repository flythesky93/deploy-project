package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Response struct {
	Code    int64  `json:"code"`
	Message string `json:"message"`
	Data    any    `json:"data"`
}

func main() {
	log.Println("Server start listen in port 20001")
	resSuccess := Response{
		Code:    200,
		Message: "Success!",
		Data:    nil,
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Call default api")
		data, err := json.Marshal(resSuccess)
		if err != nil {
			data = []byte("{\"code\":\"400\",\"message\":\"Fail!\"}")
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	})

	http.HandleFunc("/hi", func(w http.ResponseWriter, r *http.Request) {
		data, err := json.Marshal(resSuccess)
		if err != nil {
			data = []byte("{\"code\":\"400\",\"message\":\"Fail!\"}")
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	})

	log.Fatal(http.ListenAndServe(":20001", nil))
}
